#ifndef SALARMCLOCK_BASICTIME_H
#define SALARMCLOCK_BASICTIME_H
#include "TimeDate.cpp"

class BasicDate {
public:
    int hour;
    int minute;
    int weekday;
    BasicDate() = default;
    BasicDate(int hour, int minute, int weekday) {
    this->hour = hour;
    this->minute = minute;
    this->weekday = weekday;
    }
    BasicDate(TimeDate& date) {
    hour = date.hour;
    minute = date.minute;
    weekday = date.weekday;
    }
    bool operator ==(BasicDate const &other) const {
    if (hour != other.hour) return false;
    else if (minute != other.minute) return false;
    else if (weekday != other.weekday) return false;
    else return true;
    }
    void print() {
    cout << intToWeekday(weekday);
    if(hour < 10) printf("0");
    printf("%d", hour);
    printf(":");
    if(minute < 10) printf("0");
    printf("%d", minute);
    }
};
#endif //SALARMCLOCK_BASICTIME_H