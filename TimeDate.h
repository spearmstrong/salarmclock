//
// Created by spear on 4/8/2022.
//

#ifndef SALARMCLOCK_TIMEDATE_H
#define SALARMCLOCK_TIMEDATE_H
#include <string>
#include <iostream>
using namespace std;
uint8_t monthToInt(const string &monthS);
string intToMonth(uint8_t month);
string intToWeekday(uint8_t weekday);
class TimeDate {
public:
    uint8_t day;
    uint8_t month;
    uint16_t year;
    uint8_t hour;
    uint8_t minute;
    uint8_t second;
    uint8_t weekday;
    TimeDate() {
    time_t rawtime;
    time(&rawtime);
    struct tm* timeinfo = localtime(&rawtime);
    TimeDate obj(asctime(timeinfo));
    *this = obj;
    }
    TimeDate(uint8_t day1, uint8_t month1, uint16_t year1, uint8_t hour1, uint8_t min1, uint8_t sec1) {
        day = day1;
        month = month1;
        year = year1;
        hour = hour1;
        minute = min1;
        second = sec1;
        weekday = calculateWeekday();
    }
    explicit TimeDate(string info) {
        info.erase(0, 4);
        string monthS = info.substr(0,3);
        info.erase(0, 4);
        string dayS = info.substr(0,2);
        info.erase(0, 3);
        string hourS = info.substr(0,2);
        info.erase(0, 3);
        string minuteS = info.substr(0,2);
        info.erase(0, 3);
        string secS = info.substr(0,2);
        info.erase(0, 3);
        string yearS = info.substr(0,4);
        if(dayS.at(0) == ' ') {
            dayS.erase(0, 1);
        }
        day = stoi(dayS);
        month = monthToInt(monthS);
        year = stoi(yearS);
        hour = stoi(hourS);
        minute = stoi(minuteS);
        second = stoi(secS);
        weekday = calculateWeekday();
    }
    TimeDate(const TimeDate &obj) {
        day = obj.day;
        month = obj.month;
        year = obj.year;
        hour = obj.hour;
        minute = obj.minute;
        second = obj.second;
        weekday = obj.weekday;
    }
    void print() const;
    //return 0 for ==. -1 for sooner in time, 1 for rhs sooner in time
    int compare(const TimeDate &info2) const;
    int softCompare(const TimeDate &info2) const;
    string toString() const;
    bool operator <(TimeDate const &other) const {return this->compare(other) == -1; }
    bool operator >(TimeDate const &other) const {return this->compare(other) == 1; }
    bool operator <=(TimeDate const &other) const {return this->compare(other) != 1; }
    bool operator >=(TimeDate const &other) const {return this->compare(other) != -1; }
    bool operator ==(TimeDate const &other) const {return this->compare(other) == 0; }
    bool operator !=(TimeDate const &other) const {return this->compare(other) != 0; }
    TimeDate& operator=(const TimeDate &obj) {
        if(this == &obj) return *this;
        day = obj.day;
        month = obj.month;
        year = obj.year;
        hour = obj.hour;
        minute = obj.minute;
        second = obj.second;
        weekday = obj.weekday;
        return *this;
    }
    void dateFromWeekday(string weekday);
    void dateFromWeekday(string weekday, const TimeDate& date);
    void dateFromWeekday(string weekday, bool roll);
    void addDay(int integer);
    void addYear(int integer);
    void addHour(int integer);
    void addMinute(int integer);
    void addSecond(int integer);
private:
    uint8_t calculateWeekday() const;
};


#endif //SALARMCLOCK_TIMEDATE_H