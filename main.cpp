#include "main.h"
#include <thread>
#include <iostream>
using namespace std;

int main() {
    string path = "text.txt";
    Alarms newAlarmSet(path);
    ///mainly testing, not final main loop
    TimeDate time1;
    newAlarmSet.clearDatabase();
    newAlarmSet.clearAlarms();
    //newAlarmSet.newAlarm(time1);
    //thread alarm(alarmHandler, ref(newAlarmSet));
    //newAlarmSet.newAlarm(time1);
    vector<string> weekdays = {"wednesday", "tues"};
    newAlarmSet.schedule.addDates(23, 45, weekdays);
    newAlarmSet.generateFromSchedule();
    newAlarmSet.printDatabase();
    newAlarmSet.clearDatabase();
    //alarm.join();
}

string getTime() {
    time_t rawtime;
    time(&rawtime);
    struct tm* timeinfo = localtime(&rawtime);
    return asctime(timeinfo);
}

void alarmHandler(Alarms& alarms) {
    bool alarmTime = false;
    while(!alarmTime) {
        alarms.alarmM.lock();
        TimeDate currentTime(getTime());
        if(alarms.numOfAlarms() != 0) {
            if(alarms.getSoonest().softCompare(currentTime) == 0) { 
                alarmTime = true;
                alarms.alarmM.unlock();
            } else {
                alarms.alarmM.unlock();
                _sleep(900);
            }
        } else {
        alarms.alarmM.unlock();
        _sleep(900);
        }
    }
    alarmTriggered(alarms, alarms.getSoonest());
}

void alarmTriggered(Alarms& alarms, TimeDate& triggeredAlarm) {
    bool snooze = false;
    bool beepBeep = true;
    while (beepBeep) {
        _sleep(1000);
        cout << "BEEP BEEP" << endl;
        fflush (stdout);
        beepBeep = false;
    }
    alarms.removeAlarm(triggeredAlarm);
    if(snooze) {
        triggeredAlarm.addMinute(5);
        alarms.newAlarm(triggeredAlarm);
    } else {
        alarms.generateFromSchedule();
    }
    alarms.alarmM.unlock();
}