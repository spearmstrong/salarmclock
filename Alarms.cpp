//
// Created by spear on 4/8/2022.
//
#include "Alarms.h"
#include <algorithm>
void Alarms::newAlarm(string &newAlarmS) {
    alarmM.lock();
    TimeDate newTime(newAlarmS);
    if(isAlarm(newTime)) return;
    alarms.push_back(newTime);
    sort(alarms.begin(), alarms.end());
    if (path != "") backupAlarms();
    alarmM.unlock();
}
void Alarms::newAlarm(uint8_t day1, uint8_t month1, uint16_t year1, uint8_t hour1, uint8_t min1, uint8_t sec1) {
    TimeDate newTime(day1, month1, year1, hour1, min1, sec1);
    alarmM.lock();
    if(isAlarm(newTime)) return;
    alarms.push_back(newTime);
    sort(alarms.begin(), alarms.end());
    if (path != "") backupAlarms();
    alarmM.unlock();
}
void Alarms::newAlarm(TimeDate &obj) {
    alarmM.lock();
    if(isAlarm(obj)) return;
    alarms.push_back(obj);
    sort(alarms.begin(), alarms.end());
    if (path != "") backupAlarms();
    alarmM.unlock();
}
int Alarms::numOfAlarms() const {
    return alarms.size();
}
void Alarms::removeAlarm(TimeDate &obj) {
    for (auto it = alarms.begin(); it != alarms.end(); it++) {
        if(*it == obj) {
            alarms.erase(it);
            break;
        }
    }
}
void Alarms::clearAlarms() {
    alarms.clear();
}
bool Alarms::isAlarm(TimeDate &obj) const {
    for (auto it= alarms.begin(); it != alarms.end(); it++) {
        if(*it == obj) {
            return true;
        }
    }
    return false;
}
TimeDate Alarms::getSoonest() const {
    return *alarms.begin();
}

void Alarms::print() const {
    for (auto & alarm : alarms) {
        alarm.print();
    }
}
void Alarms::readAlarms() {
    fstream file;
    file.open(path, ios::in);
    string line;
    //int filesize = file.tellg();
    //line.reserve(filesize);
    //getline(file, line);
    if(!getline(file, line)) {
        file.close();
        file.open(path, ios::out);
        file<<"END\n";
        file.close();
        file.open(path, ios::in);
    }
    else {
        while (line.find("END") == string::npos) {
            TimeDate newDate(line);
            alarms.emplace_back(newDate);
            getline(file, line);
        }
        file.close();
    }
}
void Alarms::backupAlarms() const {
    clearDatabase();
    fstream file;
    file.open(path, ios::out);
    for (auto & alarm : alarms) {
        file<<alarm.toString();
        file<<"\n";
    }
    file<<"END\n";
    file.close();
}
void Alarms::printDatabase() const {
    fstream file;
    string line;
    file.open(path, ios::in);
    while (getline(file, line)) {
        cout << line << endl;
    }
}
void Alarms::clearDatabase() const {
    fstream file;
    file.open(path, ios::out | ios::trunc);
    file.close();
}

void Alarms::generateFromSchedule() {
    alarmM.lock();
    schedule.schedulerM.lock();
    TimeDate currentTimeDate;
    for (auto i = schedule.dates.begin(); i != schedule.dates.end(); i++) {
        TimeDate nextDate(currentTimeDate);
        if(i->weekday == nextDate.weekday && (i->hour > nextDate.hour || (i->hour == nextDate.hour && i->minute >= nextDate.minute))) 
            nextDate.dateFromWeekday(intToWeekday(i->weekday), false);
        else nextDate.dateFromWeekday(intToWeekday(i->weekday), true);
        nextDate.hour = i->hour;
        nextDate.minute = i->minute;
        nextDate.second = 0;
        if(!isAlarm(nextDate)) alarms.push_back(nextDate);
    }
    sort(alarms.begin(), alarms.end());
    if (path != "") backupAlarms();
    schedule.schedulerM.unlock();
    alarmM.unlock();
}