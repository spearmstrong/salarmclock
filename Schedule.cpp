#include "Schedule.h"
void Schedule::addDates(vector<BasicDate> dates) {
    schedulerM.lock();
    for (auto i = dates.begin(); i != dates.end(); i++) {
        this->dates.push_back(*i);
    }
    schedulerM.unlock();
 }
 
 void Schedule::addDates(int hour, int minute, vector<string> weekdays) {
    schedulerM.lock();
    for (auto i = weekdays.begin(); i != weekdays.end(); i++) {
        BasicDate newDate(hour, minute, stringToWeekdayInt(*i));
        dates.push_back(newDate);
    }  
    schedulerM.unlock();
 }

void Schedule::clearDates() {
    schedulerM.lock();
    dates.clear();
    schedulerM.unlock();
}
void Schedule::removeDates(vector<BasicDate> dates) {
    schedulerM.lock();
    for (auto i = dates.begin(); i != dates.end(); i++) {
        for(auto j = this->dates.begin(); i != this->dates.end(); i++) {
            if (*i == *j) {
                this->dates.erase(j);
                break;
            }
        }
    }
    schedulerM.unlock();
}

void Schedule::removeDates(int hour, int minute, vector<string> weekdays) {
    schedulerM.lock();
    vector<BasicDate> datesToRemove;
    for (auto i = weekdays.begin(); i != weekdays.end(); i++) {
        TimeDate newTimeDate;
        newTimeDate.dateFromWeekday(*i);
        newTimeDate.hour = hour;
        newTimeDate.minute = minute;
        BasicDate newDate(newTimeDate);
        datesToRemove.push_back(newDate);
    }
    for (auto i = datesToRemove.begin(); i != datesToRemove.end(); i++) {
        for(auto j = this->dates.begin(); i != this->dates.end(); i++) {
            if (*i == *j) {
                this->dates.erase(j);
                break;
            }
        }
    }
    schedulerM.unlock();
}

void Schedule::print() {
    schedulerM.lock();
    for (auto i = dates.begin(); i != dates.end(); i++) {
        i->print();
    }
    schedulerM.unlock();
}