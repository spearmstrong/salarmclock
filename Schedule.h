#ifndef SALARMCLOCK_SCHEDULE_H
#define SALARMCLOCK_SCHEDULE_H
#include <vector>
#include <mutex>
#include "BasicTime.h"

class Schedule {
public:
    mutex schedulerM;
    vector<BasicDate> dates;
    Schedule() = default;
    Schedule(const Schedule& obj) {
        schedulerM.lock();
        this->dates = obj.dates;
        schedulerM.unlock();
    }
    Schedule& operator=(const Schedule &obj) {
        if(this == &obj) return *this;
        dates = obj.dates;
        return *this;
    }
    void addDates(vector<BasicDate> dates);
    void addDates(int hour, int minute, vector<string> weekdays);
    void clearDates();
    void removeDates(vector<BasicDate> dates);
    void removeDates(int hour, int minute, vector<string> weekdays);
    void print();
};

#endif //SALARMCLOCK_SCHEDULE_H