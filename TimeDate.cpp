//
// Created by spear on 4/8/2022.
//
#include "TimeDate.h"
#include <assert.h>
#include <vector>
///credit: https://artofmemory.com/blog/how-to-calculate-the-day-of-the-week/ for weekday algorithm

void TimeDate::print() const {
    cout << intToWeekday(weekday);
    cout << ' ' << intToMonth(month) << ' ';
    if(day < 10) printf("0");
    printf("%d ", day);
    if(hour < 10) printf("0");
    printf("%d", hour);
    printf(":");
    if(minute < 10) printf("0");
    printf("%d", minute);
    printf(":");
    if(second < 10) printf("0");
    printf("%d ", second);
    printf("%d\n", year);
}
string TimeDate::toString() const {
    string out;
    out.append(intToWeekday(weekday));
    out.push_back(' ');
    out.append(intToMonth(month));
    out.push_back(' ');
    if(day < 10) out.push_back('0');
    out.append(to_string(day));
    out.push_back(' ');
    if(hour < 10) out.push_back('0');
    out.append(to_string(hour));
    out.push_back(':');
    if(minute < 10) out.push_back('0');
    out.append(to_string(minute));
    out.push_back(':');
    if(second < 10) out.push_back('0');
    out.append(to_string(second));
    out.push_back(' ');
    out.append(to_string(year));
    return out;
}
uint8_t monthToInt(const string &str) {
    if (str == "Jan") return 1;
    if (str == "Feb") return 2;
    if (str == "Mar") return 3;
    if (str == "Apr") return 4;
    if (str == "May") return 5;
    if (str == "Jun") return 6;
    if (str == "Jul") return 7;
    if (str == "Aug") return 8;
    if (str == "Sep") return 9;
    if (str == "Oct") return 10;
    if (str == "Nov") return 11;
    if (str == "Dec") return 12;
    assert(1 == 0 && "month str not a month");
    return 0;
}
string intToMonth(const uint8_t month) {
    string str;
    switch (month) {
        case 1: return str.append("Jan");
        case 2: return str.append("Feb");
        case 3: return str.append("Mar");
        case 4: return str.append("Apr");
        case 5: return str.append("May");
        case 6: return str.append("Jun");
        case 7: return str.append("Jul");
        case 8: return str.append("Aug");
        case 9: return str.append("Sep");
        case 10: return str.append("Oct");
        case 11: return str.append("Nov");
        case 12: return str.append("Dec");
        default: assert("month int not a month");
    }
    return str;
}
string intToWeekday(const uint8_t weekday) {
    string out;
    switch(weekday) {
        case 0: return out.append("Sun");
        case 1: return out.append("Mon");
        case 2: return out.append("Tues");
        case 3: return out.append("Weds");
        case 4: return out.append("Thurs");
        case 5: return out.append("Fri");
        case 6: return out.append("Sat");
        default: assert("weekday int not a weekday");
    }
    return out;
}
int stringToWeekdayInt(string weekday) {
    for (auto i = weekday.begin(); i != weekday.end(); i++) *i = tolower(*i);
    if (weekday == "mon" || weekday == "monday") return 1;
    else if (weekday == "tues" || weekday == "tuesday") return 2;
    else if (weekday == "weds" || weekday == "wednesday") return 3;
    else if (weekday == "thurs" || weekday == "thursday") return 4;
    else if (weekday == "fri" || weekday == "friday") return 5;
    else if (weekday == "sat" || weekday == "saturday") return 6;
    else if (weekday == "sun" || weekday == "sunday") return 0;
    else assert("error: weekday string must be equal to mon, monday, tue, tuesday, wed, wednesday, thur, thursday, fri, friday, sat, saturday, sun, or sunday.");
    return -1;
}
void collectDigits(vector<uint16_t> &digits, const uint16_t &x) {
    vector<uint16_t> reverseD;
    for (int i = x; i != 0; i = i/10) {
        reverseD.push_back(i%10);
    }
    for (auto it = reverseD.rbegin(); it != reverseD.rend(); it++) {
        digits.push_back(*it);
    }
}
int monthToMonthCode(const uint8_t month) {
    switch(month) {
        case 1: return 0;
        case 2: return 3;
        case 3: return 3;
        case 4: return 6;
        case 5: return 1;
        case 6: return 4;
        case 7: return 6;
        case 8: return 2;
        case 9: return 5;
        case 10: return 0;
        case 11: return 3;
        case 12: return 5;
        default: assert("month int not a month");
    }
    return -1;
}
int centuryToCenturyCode(const vector<uint16_t> &vec) {
    int century = (vec.at(0) * 10) + vec.at(1);
    switch(century) {
        case 17: return 4;
        case 18: return 2;
        case 19: return 0;
        case 20: return 6;
        case 21: return 4;
        case 22: return 2;
        case 23: return 0;
        default: {
            assert("century found not covered by the weekday algorithm");
            return -1;
        }
    }
}
uint8_t TimeDate::calculateWeekday() const {
    int yearCode, yy, monthCode, centCode, leapCode;
    vector<uint16_t> yearDigits;
    collectDigits(yearDigits, year);
    yy = (yearDigits.at(2) * 10) + yearDigits.at(3);
    yearCode = (yy +(yy/4)) % 7;
    monthCode = monthToMonthCode(month);
    centCode = centuryToCenturyCode(yearDigits);
    leapCode = 0;
    if (year/4 == 0 && year/100 != 0 || year/400 == 0) {
        if (month == 1 || month == 2) {
            leapCode = -1;
        }
    }
    return (yearCode + monthCode + centCode + day - leapCode) % 7;
}
int TimeDate::compare(const TimeDate &other) const {
    //return 0 for ==. -1 for sooner in time, 1 for rhs sooner in time
    if(year < other.year) return -1;
    else if (year > other.year) return 1;
    if(month < other.month) return -1;
    else if (month > other.month) return 1;
    if(day < other.day) return -1;
    else if (day > other.day) return 1;
    if(hour < other.hour) return -1;
    else if (hour > other.hour) return 1;
    if(minute < other.minute) return -1;
    else if (minute > other.minute) return 1;
    if(second < other.second) return -1;
    else if (second > other.second) return 1;
    return 0;
}
int TimeDate::softCompare(const TimeDate &other) const {
    //return 0 for ==. -1 for sooner in time, 1 for rhs sooner in time
    if(year < other.year) return -1;
    else if (year > other.year) return 1;
    if(month < other.month) return -1;
    else if (month > other.month) return 1;
    if(day < other.day) return -1;
    else if (day > other.day) return 1;
    if(hour < other.hour) return -1;
    else if (hour > other.hour) return 1;
    if(minute < other.minute) return -1;
    else if (minute > other.minute) return 1;
    return 0;
}
void TimeDate::addDay(int integer) {
    int newDay = day + integer;
    bool isLeapYear = (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0));
    bool isValidDay = (!(newDay > 28 && month == 2 && !isLeapYear) && !(newDay > 29 && month == 2 && isLeapYear) &&
    !(newDay > 30 && (month == 4 || month == 6 || month == 9 || month == 11)) && 
    !(newDay > 31) && !(newDay < 1));
    while(!isValidDay) {
        if(newDay > 28 && month == 2 && !isLeapYear) {
            month++;
            newDay -= 28;
        }
        else if(newDay > 29 && month == 2 && isLeapYear) {
            month++;
            newDay -= 29;
        }
        else if(newDay > 30 && (month == 4 || month == 6 || month == 9 || month == 11)){
            month++;
            newDay -= 30;
        }
        else if(newDay > 31 && month != 12) {
            month++;
            newDay -= 31;
        } else if (newDay>31 && month == 12) {
            month = 1;
            this->addYear(1);
            newDay -= 31;
        }
        else {  //day < 1
            month -= 1;
            if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) newDay = 31 + newDay;
            else if(month == 4 || month == 6 || month == 9 || month == 11) newDay = 30 + newDay;
            else if(month == 2 && !isLeapYear) newDay = 28 + newDay;
            else if(month == 2 && isLeapYear) newDay = 29 + newDay;
            else { //month went to zero, roll over
            this->addYear(-1);
            month = 12;
            newDay = 31 + newDay;
            }
        }
        isValidDay = (!(newDay > 28 && month == 2 && !isLeapYear) && !(newDay > 29 && month == 2 && isLeapYear) &&
        !(newDay > 30 && (month == 4 || month == 6 || month == 9 || month == 11)) && 
        !(newDay > 31) && !(newDay < 1));
    } 
    assert(newDay > 0);
    assert(newDay < 32);
    day = newDay;
    weekday = this->calculateWeekday();
}
void TimeDate::addYear(int integer) {
    int total = year + integer;
    assert(total >= 0 && "ERROR: YEAR CANNOT BE BELOW ZERO");
    if(total > -1) year = total;
}
void TimeDate::addHour(int integer) {
    int diff = hour + integer;
    while(diff > 23 || diff < 0) {
        if(hour + integer > 23) {
            this->addDay(1);
            integer -= 24;
        } else {
            this->addDay(-1);
            integer += 24;
        }
    }
    hour += integer;
}
void TimeDate::addMinute(int integer) {
    int diff = minute + integer;
    while (diff > 59 || diff < 0) {
        if(diff > 59) {
        this->addHour(1);
        integer -= 60;
        } else {
            this->addHour(-1);
            integer += 60;
        }
    }
    minute += integer;
}

void TimeDate::addSecond(int integer) {
    int diff = second + integer;
    while (diff > 59 || diff < 0) {
        if(diff > 59) {
        this->addMinute(1);
        integer -= 60;
        } else {
            this->addMinute(-1);
            integer += 60;
        }
    }
    second += integer;
}

void TimeDate::dateFromWeekday(string weekday) {
    int weekdayNum = stringToWeekdayInt(weekday);
    TimeDate date;
    int differenceNum = 0;
    if(weekdayNum <= date.weekday) weekdayNum += 7;
    differenceNum = weekdayNum - date.weekday;
    date.addDay(differenceNum);
    *this = date; 
}
//false is same day, true is roll forward
void TimeDate::dateFromWeekday(string weekday, bool roll) {
    int weekdayNum = stringToWeekdayInt(weekday);
    TimeDate date;
    int differenceNum = 0;
    if(weekdayNum <= date.weekday && roll) weekdayNum += 7;
    differenceNum = weekdayNum - date.weekday;
    date.addDay(differenceNum);
    *this = date; 
}
void TimeDate::dateFromWeekday(string weekday, const TimeDate& date) {
    int weekdayNum = stringToWeekdayInt(weekday);
    int differenceNum;
    *this = date;
    if(weekdayNum < this->weekday) weekdayNum + 6;
    differenceNum = weekdayNum - this->weekday;
    this->addDay(differenceNum);
}