//
// Created by spear on 4/8/2022.
//
#include <string>
#include <ctime>
#include <iostream>
#include "Alarms.cpp"
using namespace std;

#ifndef SALARMCLOCK_MAIN_H
#define SALARMCLOCK_MAIN_H

string getTime();
void alarmInterrupt(int sig);
void alarmHandler(Alarms& alarms);
void alarmTriggered(Alarms& alarms, TimeDate triggeredAlarm);

#endif //SALARMCLOCK_MAIN_H