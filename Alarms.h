//
// Created by spear on 4/8/2022.
//

#ifndef SALARMCLOCK_ALARMS_H
#define SALARMCLOCK_ALARMS_H
#include <fstream>
#include <vector>
#include <mutex>
//#include "TimeDate.cpp"
#include "Schedule.cpp"
using namespace std;
class Alarms { 
public:
    Schedule schedule;
    mutex alarmM;
private:
    string path;
    vector<TimeDate> alarms;
public:
    Alarms() = default;
    explicit Alarms(string &path2) {
        alarmM.lock();
        path = path2;
        readAlarms();
        alarmM.unlock();
    }
    Alarms(const Alarms& obj) {
        schedule = obj.schedule;
        path = obj.path;
        alarms = obj.alarms;        
    }
    void newAlarm(string &info);
    void newAlarm(uint8_t day1, uint8_t month1, uint16_t year1, uint8_t hour1, uint8_t min1, uint8_t sec1);
    void newAlarm(TimeDate &obj);
    bool isAlarm(TimeDate &obj) const;
    void generateFromSchedule();
    int numOfAlarms() const;
    void removeAlarm(TimeDate &obj);
    void clearAlarms();
    TimeDate getSoonest() const;
    void print() const;
    void printDatabase() const;
    void clearDatabase() const;

private:
    void readAlarms();
    void backupAlarms() const;
};


#endif //SALARMCLOCK_ALARMS_H